from wand.image import Image as wi
from PIL import Image
import io
from pyzbar.pyzbar import decode


def qrs_from_image(image, bin_thresh=200):
    """Extracts QRs from single image

    Args:
        image (PIL.Image): Image
        bin_thresh (int, optional): Binarization threshold. Defaults to 200.

    Returns:
        list: list of QRs
    """

    # Convert to binary black/white image
    # Significantly improves QR decoding performance
    # Binarization taken from
    # https://stackoverflow.com/questions/9506841/using-python-pil-to-turn-a-rgb-image-into-a-pure-black-and-white-image
    def binarization_fn(x):
        return 255 if x > bin_thresh else 0

    # Convert first to grayscale (8bit, 'L'), then to binary
    bw = image.convert('L').point(binarization_fn, mode='1')

    # Decode QR
    qrs = decode(bw)

    # Parse to ASCII strings
    if qrs:
        qr_strings = []
        for qr in qrs:
            qr_string = qr.data.decode('ascii')
            qr_strings.append(qr_string)

    else:
        qr_strings = ['']

    return qr_strings


def qrs_from_pdf_page(page, bin_thresh=200):
    # Open as wandimage (as Pillow is not able to read PDFs)
    wimage = wi(image=page)

    # Convert to Pillow
    i = Image.open(io.BytesIO(wimage.make_blob("png")))

    # Extract all QRs on current page
    qrs = qrs_from_image(image=i, bin_thresh=bin_thresh)

    return qrs


def first_qr_from_first_pdf_page(pdf_file, dpi=150, bin_thresh=200):
    PDFfile = wi(filename=pdf_file + "[0]", resolution=dpi)

    qrs = qrs_from_pdf_page(page=PDFfile.sequence[0], bin_thresh=bin_thresh)
    return qrs[0]


def qrs_from_pdf(pdf_file, return_first=False, dpi=150, bin_thresh=200):
    """Extracts all QRs in a PDF file

    Args:
        pdf_file (str): path of PDF file
        return_first (bool): if set, find first qr and skip rest
        dpi (int): dots per inch
        bin_thresh (int): threshold for binarization (between 0 and 255)

    Returns:
        list: decoded QR code strings per page
        int: number of pages
    """

    # Open PDF
    PDFfile = wi(filename=pdf_file, resolution=dpi)
    numpages = len(PDFfile.sequence)

    # Loop over PDF pages
    qrs_pages = []  # QR strings per page
    qrs_seq = []  # All QR strings as a sequence
    for page_no, page in enumerate(PDFfile.sequence):
        # Extract QRs from PDF page
        qrs = qrs_from_pdf_page(page=page, bin_thresh=bin_thresh)

        # If return first enabled
        # check if any QR was found and return
        if return_first and any(qrs):
            first_qr = next(_ for _ in qrs if _)
            return first_qr, -1

        # Cannot handle more than one QR per page yet
        qrs_pages.append(qrs)
        qrs_seq.extend(qrs)

    # Post check
    if return_first:
        if not any(qrs_seq):
            return "", -1

    return qrs_pages, numpages
