import os


def find_file(pattern, path):
    """Finds file given pattern

    Args:
        pattern (str): pattern
        path (str): path to folder

    Returns:
        list: list of filenames in folder matching pattern
    """

    import fnmatch

    result = []
    for root, _, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))

    return result


def _extract_matnum(s):
    """Internal wrapper for splitting string

    Args:
        s (string): string

    Returns:
        string: all characters in string until first occurence of '_'.
    """
    # Has to be separated by "_" from the rest of the file name
    return s.split('_', 1)[0]


def check_matnum(matnum):
    """Checks for valid matriculation number

    * All characters have to be digits
    * Either five or six digits

    Args:
        matnum (str): matriculation number

    Returns:
        bool: valid
    """

    return (len(matnum) == 6 or len(matnum) == 5) and matnum.isdigit()


def starts_with_matnum(s):
    """Checks string for starting with valid matriculation number

    Args:
        s (str): file name with first 5 to 6 characters matriculation number

    Returns:
        bool: valid
    """

    matnum = _extract_matnum(s)
    return check_matnum(matnum)


def get_matnum(s):
    """Extracts matriculation number from string and checks if valid

    Args:
    s (str): file name with first 6 characters matriculation number

    Returns:
    str: 6-digit matriculation number

    Throws:
    Error (ValueError) if file does not start with propper matriculation number
    """

    # Get matriculation number
    matnum = _extract_matnum(s)

    # Sanity check
    if not check_matnum(matnum):
        raise ValueError("{} not a valid matriculation number".format(matnum))

    return matnum
