import unittest
import time
import os
import tempfile


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_copy_from_zip(self):
        import handlemoodlesubmissions

        expected_files = [
            '123001_L.pdf',
            '123002_L.pdf',
            '123010_L.pdf',
            '123011_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions.zip'
        sheet_csv = "./tests/assets/Grades.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Call function
        handlemoodlesubmissions.main([
            in_zip, sheet_csv, out_dir, "-t", tmp_dir])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)
