import unittest
import time
import os
import tempfile
import shutil


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_batch(self):
        import batch

        expected_files = [
            'moodle_feedbacks.zip',
            'passwords.csv']
        expected_folders = [
            'LastnameA, FirstnameA_1519321_assignsubmission_file_',
            'LastnameB, FirstnameB_1519322_assignsubmission_file_',
            'LastnameJ, FirstnameJ_1519330_assignsubmission_file_',
            'LastnameK, FirstnameK_1519331_assignsubmission_file_'
        ]
        expected_pdfs = [
            '123001_LastnameA_w_aes.pdf']

        # Prepare parameter
        in_dir = './tests/assets/pdfs'
        csv = './tests/assets/Grades.csv'
        dpi = 50

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        zipout_dir = os.path.join(self.test_dir, 'zipout')
        os.mkdir(zipout_dir)

        # Copy supplements file
        batch.main([
            in_dir, csv, out_dir,
            "-t", tmp_dir, "--cores", "1", "--dpi", str(dpi)])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

        # Unpack zip
        zipfullfile = os.path.join(out_dir, 'moodle_feedbacks.zip')
        shutil.unpack_archive(zipfullfile, zipout_dir)

        # Assert zip output
        created_folders = os.listdir(zipout_dir)
        created_folders.sort()
        self.assertEqual(expected_folders, created_folders)

        created_pdfs = os.listdir(os.path.join(zipout_dir, created_folders[0]))
        self.assertEqual(created_pdfs, expected_pdfs)
