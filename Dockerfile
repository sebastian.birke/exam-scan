# Ubuntu 20.04 (focal)
# https://hub.docker.com/_/ubuntu/?tab=tags&name=focal
# OS/ARCH: linux/amd64
ARG ROOT_CONTAINER=ubuntu:focal-20210401
ARG BASE_CONTAINER=$ROOT_CONTAINER
FROM $BASE_CONTAINER

LABEL maintainer="Christian Rohlfing <rohlfing@ient.rwth-aachen.de>"

# Install Ubuntu packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
 && apt-get install -yq --no-install-recommends \
    wget \
    libmagickwand-dev \
    ghostscript \
    libzbar0 \
    python3-pip \
    python3-setuptools \
 && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install Python packages
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install setuptools
RUN pip3 install -r /tmp/requirements.txt

# ImageMagick hack
# taken from https://stackoverflow.com/questions/53377176/change-imagemagick-policy-on-a-dockerfile
# Add PDF read|write rights
ARG imagemagic_policy=/etc/ImageMagick-6/policy.xml
RUN sed -i 's/<policy domain="coder" rights="none" pattern="PDF" \/>/<policy domain="coder" rights="read|write" pattern="PDF" \/>/g' $imagemagic_policy

# Create non-root user
RUN useradd -m friendlyscanner
USER friendlyscanner

# Copy all files
COPY --chown=friendlyscanner . /app
WORKDIR /app 

# Entrypoint is python
ENTRYPOINT ["python3"]

# Standard command is batch.py
CMD ["./batch.py", "--help"]
