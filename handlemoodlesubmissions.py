#!/usr/bin/env python

"""Extract student's submission files from Moodle assignment

Transfer PDF files from ZIP file containing all submissions of a Moodle
assignment into output folder with file names following exam scan
naming convention.

Attention: Contents in output folder will be overwritten in the following!
"""

__author__ = "Amrita Deb (deb@itc.rwth-aachen.de), " +\
    "Christian Rohlfing (rohlfing@ient.rwth-aachen.de)"


import sys  # get arguments from command line
import os  # path listing/manipulation/...
import time  # keep track of time
import argparse  # handle command line arguments
import shutil  # unzipping and copying files

from utils import moodle as moodle


def _make_parser():
    csv_parser = moodle.get_moodle_csv_parser()

    parser = argparse.ArgumentParser(
        parents=[csv_parser],
        description=__doc__, prog='handlemoodlesubmissions.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "inzip", help="Input ZIP file or extracted folder.")
    parser.add_argument(
        "csv", help="Moodle grading sheet.")
    parser.add_argument(
        "outfolder", help="Output folder with PDFs.")

    parser.add_argument(
        "-f", "--filenameformat", default="{matnum}_{fullname[0]}",
        help="File name format. Available keywords: " +
        "{matnum}, {fullname}, {lastname}, {firstname}. " +
        "Default: '{matnum}_{fullname[0]}'")
    parser.add_argument(
        "-c", "--copyall", action='store_true',
        help="If set, copies all files (including multiple and non-PDF files)")
    parser.add_argument(
        "-a", "--appendoriginal", action='store_true',
        help="If set, appends original file name to new location's file name")
    parser.add_argument(
        "-d", "--dry", action='store_true', help="Flag for dry run.")
    parser.add_argument(
        "-t", "--tmp", default="./tmp", help="Temporary folder.")

    return parser


# Create argument parser with default values
_parser = _make_parser()


def main(args):
    """Main routine

    1) Files are extracted from zip file location eg: ./all_submissions.zip
        In case folder is given, extraction is skipped.
    2) Scan through extracted folder for PDF files.
        Only 1 PDF file/student is accepted.
    3) Matriculation number and last name are fetched from grading worksheet
    4) PDFs from extracted folder are renamed according to convention and
        placed in user provided outfolder
    """

    # Argument handling
    args = _parser.parse_args(args)
    inzip = args.inzip
    outfolder = args.outfolder
    sheet_csv = args.csv
    dry = args.dry
    csv_enc = args.csvenc
    csv_delim = args.csvdelim
    csv_quote = args.csvquote
    copy_all = args.copyall
    append_original_name = args.appendoriginal
    filenameformat = args.filenameformat
    tmp_folder = args.tmp
    extracted_folder = os.path.join(tmp_folder, "extracted_from_moodle")

    # Check folders
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)

    # Print status
    starttime = time.time()
    num_students = moodle.get_student_number(sheet_csv=sheet_csv,
                                             csv_enc=csv_enc)

    print('''Preparing for renaming of submission files.
Processing {} students
  '''.format(num_students))

    # Clean up and create temporary folder
    dryout = []
    if dry:
        print("Dry run\n")

    # Check whether zip or folder is given
    folder_instead_of_zip = False
    if not(inzip.lower().endswith(('.zip'))):
        if not(os.path.isdir(inzip)):
            raise Exception(
                "{zip} neither Zip file nor folder. Exiting."
                .format(zip=inzip))
        else:
            inzip_folder = os.listdir(inzip)
            inzip_files = [_ for _ in inzip_folder if _.lower().endswith(".zip")]
            if len(inzip_files) > 0:
                print("""
Available submission file(s) to be unzipped and renamed:
- {}

Files in output folder {} will be overwritten during this process.
    """.format("\n- ".join(inzip_files),  outfolder))
            else:
                print("""
There are no zip files in the given directory.
Exiting now.""")
                return
        # Folder was given instead of Zip file
        extracted_folder = inzip
        folder_instead_of_zip = True

    else:
        # Extract
        if os.path.isfile(inzip):
            print("Extracting files from {zip} ...".format(zip=inzip))
            if not dry:
                shutil.unpack_archive(inzip, extracted_folder)  # unzip file
            else:
                raise Exception("Dry run prevents me from unpacking the Zip file.")
        else:
            print("""
There are no zip files in the given directory.
Exiting now.""")
            return


    # List all extracted folders
    print('Extracted folde '+extracted_folder)
    folders = os.listdir(extracted_folder)
    folders.sort()
    print(len(folders))

    # There should never be more folders than entries in CSV file
    if len(folders) > num_students:
        raise Exception(
            ("More folders ({num_folders}) than "
             "students in CSV file ({num_students})")
            .format(num_folders=len(folders), num_students=num_students))

    # Parse grading infos from CSV file
    infos = moodle.extract_info(sheet_csv=sheet_csv, csv_delim=csv_delim,
                                csv_quote=csv_quote, csv_enc=csv_enc)

    # Collect non-default cases:
    # Student did not submit anything
    infos_no_submission = []
    # Student submitted more than one file
    infos_multi_files = []
    # Student submitted a non-PDF file
    infos_unsupported_files = []

    # Loop over grading info
    print("Copying submissions", sep=' ', end='', flush=True)
    for cnt, info in enumerate(infos):
        folder = moodle.submission_folder_name(info)

        if folder in folders:
            # Folder was found
            folderfull = os.path.join(extracted_folder, folder)
            files = os.listdir(folderfull)

            # Notify if folder empty
            if len(files) == 0:
                infos_no_submission.append(info)

            # Notify if more than one submission
            if len(files) > 1:
                infos_multi_files.append(info)

            # Iterate over all files within folder
            for file_cnt, file in enumerate(files):
                file_full = os.path.join(folderfull, file)

                # Create destination file name
                dest = filenameformat.format(
                    matnum=info['matnum'], fullname=info['fullname'],
                    lastname=info['lastname'], firstname=info['firstname'])

                # Add unique file ID (only for copy all)
                if copy_all > 0:
                    dest = dest + "_{:03d}".format(file_cnt)

                base, ext = os.path.splitext(file)
                # Add original file name
                if append_original_name:
                    dest = dest + "_" + base

                # Add extension
                dest = dest + ext
                dest_full = os.path.join(outfolder, dest)

                # Notify if non-PDF file
                is_pdf = file_full.lower().endswith('.pdf')
                if not is_pdf and \
                        info not in infos_unsupported_files:
                    infos_unsupported_files.append(info)

                # Copy either first PDF file or all files if copyall is active
                if (file_cnt == 0 and is_pdf) or copy_all:
                    if not dry:
                        shutil.copyfile(file_full, dest_full)
                    else:
                        dryout.append(
                            "- {old} -> {new}"
                            .format(old=os.path.join(folder, file), new=dest))
        else:
            # Notify if folder was not found
            infos_no_submission.append(info)

        # Print for-loop progress
        if not (cnt % max(1, round(num_students/10))):
            print(".", sep=' ', end='', flush=True)

    print("done.")

    # Report back special cases
    for report in [(infos_no_submission, "no files"),
                   (infos_multi_files, "multiple files"),
                   (infos_unsupported_files, "unsupported files")]:
        infos, reason = report
        if len(infos) > 0:
            lines = ["- {folder} ({matnum})"
                     .format(folder=moodle.submission_folder_name(_),
                             matnum=_['matnum'])
                     for _ in infos]
            lines.sort()
            print(
                "\nSubmissions of {reason}:\n{lines}"
                .format(reason=reason, lines="\n".join(lines)))

    # Dry run output
    if not dry:
        # Delete temporary folder
        if not folder_instead_of_zip:
            shutil.rmtree(extracted_folder)
    else:
        dryout.sort()
        print("\nDry run results:\n{}".format("\n".join(dryout)))

    # Print status
    endtime = time.time()
    print("Time taken: {:.2f}".format(endtime-starttime))


if __name__ == '__main__':
    main(sys.argv[1:])
