# exam-scan

Exam-Scan is a command-line tool that helps to watermark and/or encrypt feedback files/scanned exams/additional exam materials and prepare them in Moodle uploadable format.
The tool is designed to handle zipped submissions downloadable from Moodle as well as scanned PDFs from your local scanner.

## Contents

* `handlemoodlesubmissions.py` (*for downloadable PDFs*) unzips files from submission zip file downloadable from Moodle(in case there are no scans) and renames it accordingly in the format `<Matriculation number>_<Lastname/first letter of Lastname>`
* `renamescans.py` (*for scanned PDFs*) Rename scanned PDFs, assuming scan order equal to alphabetical order of students in Moodle grading sheet, such that the file name is in the format in the format `<Matriculation number>_<first letter of Lastname>`. This only works if exams were scanned in alphabetical order. Optionally, each scanned PDF is searched for barcodes/QRs containing the matriculation number to double check.
* `supplements.py` renames and create copies of sample solutions(if any)/additional exam materials for every student.
* `watermark.py` watermarks each page of PDFs containing exam scans with matriculation number of the respective student
* `encrypt.py` encrypts PDFs either with a common password(passed as an argument) or a randomly generated password(when there is no argument)
* `preparemoodleupload.py` zips PDFs in the format acceptable for moodle upload via assign module as feedback file for each student
* `batch.py` executes all three programs as a singular batch job

Please note that `handlemoodlesubmissions.py`, `supplements.py`, `watermark.py`, `encrypt.py`, `preparemoodleupload.py` do not depend on each other.
If you want to use only a subset (or one) of the scripts, you can do so after installing the corresponding script's [dependancies](Dependancies.md).

Exemplary outputs can be downloaded:

* [moodle_feedbacks.zip](https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/jobs/artifacts/master/raw/out/moodle_feedbacks.zip?job=test): The zip-Archive to be uploaded to Moodle containing the watermarked and encrypted PDFs for each student.
* [passwords.csv](https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/jobs/artifacts/master/raw/out/passwords.csv?job=test): CSV file containing passwords for each PDF.

For more info please refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/)

## Instructions

### Prerequisites

* **Create and setup Moodle**
  * In your Moodle course room, create an `assign` module following this [guideline](https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/0cfca4212fef4712ad2d432ac83eaf3e)
  * Download the grading table `Grades.csv` from Moodle via: `View all submissions` &#8594; `Grading action` &#8594; `Download grading worksheet`

* **Create PDFs corresponding to each exam**
  * Scan the exams and save the scans as PDFs (each page should be A4). For most copy machines, you can save an A3 scan (double page of an exam) as two A4 pages.
  * You can either
      1. Order the scans following the same order as the grading worksheet
      1. Or rename the scans in the format `<Matriculation number>_<Lastname/first letter of Lastname>` (e.g. `123456_Nachname.pdf`/`123456_L.pdf`).

* **OR: Download submission zip file**
  * Download the submission zip file from (Assignment Main Page->View all submissions->Download all submissions)

* **Optional: Create sample solutions/additional exam materials (Refer [here](https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/issues/3))**
  * Scan the sample solutions/additional exam materials and save the scans as PDFs (each page should be A4). For most copy machines, you can save an A3 scan (double page of an exam) as two A4 pages.
  * Place all PDFs in a folder, e.g. `supplements`.

* **Install the software dependancies**

  The current version of code was tested on Windows10, Ubuntu 20.04.1 LTS and macOS 10.14 Mojave to ensure platform independence.The code has the following software dependencies which needs to installed before the programs can be run successfully. 
  * Imagemagick (version: 7.0.10-33)
  * Ghostscript (version: 9.53.3)
  * Python (version: 3.8/3.9)
  * PIP (version: 21.0.1)
  * Additional Python modules:
    * wand (version 0.6.5)
    * pillow (version: 8.1.0)
    * pwgen (version: 0.8.2.post0)
    * pikepdf (version 2.5.0)
    * zip (version 0.02)

  Instructions to install software dependencies based on your operating system:
  * Windows 10 : [Installation of Software Dependencies](swdependencies_win.md)
  * MacOS : [Installation of Software Dependencies](swdependencies_mac.md)
  * Linux : [Installation of Software Dependencies](swdependencies_linux.md)

 If you are an experienced user and familiar with the python `venv` (virtual environments) module and after having installed both ImageMagick (beware the `Policy Error` fix in [FAQs])and Ghostscript you can install the python dependencies in the virtual environment via pip with

  ```bash
  python -m venv venv
  source venv/bin/activate
  pip install -r requirements.txt 
  ```
### Docker

If you are an experienced user familiar with Docker, you can use the provided `Dockerfile` to easily run the scripts.
Either use the already built image

```bash
docker run --name='examscan' --rm -v $(pwd):$(pwd) -w $(pwd) registry.git.rwth-aachen.de/rwthmoodle/exam-scan:master batch.py --help
```

or build the Dockerfile yourself locally

```bash
docker build -t examscan:latest .
docker run --name examscan --rm -v $(pwd):$(pwd) -w $(pwd) examscan:latest batch.py --help
```

## Commands

#### (Online Submissions) Unzip submission files from assign module and rename them

Assuming that `./tests/assets/submissions.zip` is the zip file containing all submissions and `./tests/assets/Grades.csv` is the grading worksheet

```bash
python handlemoodlesubmissions.py ./tests/assets/submissions.zip  ./tests/assets/Grades.csv ./tests/assets/pdfs 
```

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/handlemoodlesubmissions.html)

#### (Scanned Exams) Rename scanned PDFs

Assuming that `./tests/assets/pdfs_scan` is the folder containing all scans, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/pdfs` is the output folder where all the renamed scans will be placed

```bash
python renamescans.py  ./tests/assets/pdfs_scan ./tests/assets/Grades.csv ./tests/assets/pdfs
```
For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/renamescans.html)

#### Prepare copies of Sample Solutions for each student (Optional)

Assuming that the folder `./tests/assets/supplements` holds the scans of the sample solution/additional materials, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/pdfs` is the output folder where all the renamed materials will be placed

```bash
python supplements.py ./tests/assets/supplements ./tests/assets/Grades.csv ./tests/assets/pdfs
```

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/supplements.html)

#### Watermark the submissions

Assuming that the folder `./tests/assets/pdfs` holds the scans of the exams and filenames follow the format `<Matriculation number>_<Lastname/first letter of Lastname>`, `./tests/assets/pdfs_watermarked` is the output folder where all the watermarked PDFs will be placed and cores=2 indicate the number of cores for parallel processing

```bash
python watermark.py ./tests/assets/pdfs ./tests/assets/pdfs_watermarked --cores 2 --dpi 150 --quality 75
```

**TIP:** Play around with `dpi` and `quality` parameters according to your requirements. Higher values for these two will result in high resolution PDFs of bigger size (ideal for when the number of files is low). Lower values will result in PDFs having lower file size and low resolution (ideal when the number of files is high)

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/watermark.html)

#### Encrypt the files

Assuming that the folder `./tests/assets/pdfs_watermarked` holds the files to encrypt, `./tests/assets/pdfs_encrypted` is the output folder where all the encrypted PDFs will be placed and providing a common password for all encrypted PDFs with the `--password` option

```bash
python  encrypt.py ./tests/assets/pdfs_watermarked ./tests/assets/pdfs_encrypted --password ganzgeheim
```
**TIP:** Omitting the `password` option, you can set randomly generated passwords for each PDF

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/encrypt.html)

#### Prepare for Moodle batch upload

Assuming that the folder `./tests/assets/pdfs_encrypted` holds the scans of be uploaded to Moodle, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/out` is the output folder where zip archive will be palced

```bash
python preparemoodleupload.py ./tests/assets/pdfs_encrypted  ./tests/assets/Grades.csv  ./tests/assets/out/feedback.zip
```

Then, you can upload `feedback.zip` in Moodle:
`Alle Angaben anzeigen` &#8594; `Bewertungsvorgang` &#8594; `Mehrere Feedbackdateien in einer Zip-Datei hochladen`

**NOTE:** There is a upload filesize limit in Moodle. For RWTHMoodle (on the day this README is updated), this value is 250MB. If the folder contaning the PDFs is greater than upload filesize limit, provide the limit as a parameter like `--moodleuploadlimit 250`
For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/preparemoodleupload.html)


#### Batch job

You can do all the above processes with one single scripts as follows:

Online Submissions:
```bash
python batch.py ./tests/assets/pdfs ./tests/assets/Grades.csv ./tests/assets/out --cores 2 --password ganzgeheim --suppinfolder ./tests/assets/supplements --supp --zip ./tests/assets/submissions.zip
```

Scanned Exams:
```bash
python batch.py ./tests/assets/pdfs ./tests/assets/Grades.csv ./tests/assets/out --cores 2 --password ganzgeheim --suppinfolder ./tests/assets/supplements --supp
```

In this case the `./tests/assets/out` contains both passwords.csv and zip archive
For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/batch.html)

## Original Authors

Helmut Flasche, Jens Schneider, Christian Rohlfing, IENT RWTH Aachen\
Dietmar Kahlen, ITHE RWTH Aachen\
Amrita Deb, IT Center, RWTH Aachen University

## Who do I talk to?

Servicedesk IT-Center RWTH Aachen <[servicedesk@itc.rwth-aachen.de](mailto:servicedesk@itc.rwth-aachen.de)>

If you have any errors or problems while running the programs, make sure to first check if your error is listed in [FAQs]

[FAQs]: https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/blob/master/FAQs.md
