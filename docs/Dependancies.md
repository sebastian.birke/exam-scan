# Dependencies

Note that all Python packages are listed in the file `requirements.txt`.

## `watermark.py`

* ImageMagick 7.0.10-33
* Ghostscript 9.53.3
* Pillow 8.1.0
* Wand 0.6.5

## `encrypt.py`

* pwgen 0.8.2.post0
* pikepdf 2.5.0

## `preparemoodleupload.py`

* zipfile (version 0.02)

## `renamescans.py`

* pyzbar (version: 0.1.8)