<header>

.. toctree::
  :maxdepth: 3

.. autoprogram:: <file>:_parser
  :prog: <file>.py

API
===

.. automodule:: <file>
  :members:
