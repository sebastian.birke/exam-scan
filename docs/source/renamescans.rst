Rename Scans
************

.. toctree::
  :maxdepth: 3

.. autoprogram:: renamescans:_parser
  :prog: renamescans.py

API
===

.. automodule:: renamescans
  :members:
