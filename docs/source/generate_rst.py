#!/usr/bin/env python

import os


def generateFiles(file_infos, template_path, out_dir):
    """ insert figures into latex template
    """

    with open(template_path, "r") as template_handle:
        template_string = template_handle.read()

        for file_info in file_infos:
            file = file_info['file']
            title = file_info['title']

            line = '*'*len(title)
            header = "{title}\n{line}".format(title=title, line=line)
            s = template_string.replace("<file>", file)
            s = s.replace("<header>", header)

            out_file = "{file}.rst".format(file=file)
            out_path = os.path.join(out_dir, out_file)

            if os.path.exists(out_path):
                print("Overwriting {file}."
                      .format(file=out_file))

            with open(out_path, 'w') as out_handle:
                out_handle.write(s)

        print("Done.")


if __name__ == '__main__':
    """ Main routine
    """

    file_infos = [
        {'file': 'watermark', 'title': 'Watermark'},
        {'file': 'batch', 'title': 'Batch'},
        {'file': 'encrypt', 'title': 'Encrypt'},
        {'file': 'preparemoodleupload', 'title': 'Prepare Moodle Upload'},
        {'file': 'handlemoodlesubmissions', 'title': 'Handle Moodle Submission'},
        {'file': 'renamescans', 'title': 'Rename Scans'},
        {'file': 'supplements', 'title': 'Supplements'},
    ]

    generateFiles(
        file_infos=file_infos, template_path='./docs/source/_template.rst',
        out_dir='./docs/source/')
