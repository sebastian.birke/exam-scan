Handle Moodle Submission
************************

.. toctree::
  :maxdepth: 3

.. autoprogram:: handlemoodlesubmissions:_parser
  :prog: handlemoodlesubmissions.py

API
===

.. automodule:: handlemoodlesubmissions
  :members:
